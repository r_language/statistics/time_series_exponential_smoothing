# Time_series_and_exponential_smoothing



In the R file provided you will find a time series analysis with varicelle document that you need to download. After, there is an exponential smoothing on varicelle data. Finally, there is another time series analysis with maunaloa2Co2 document that you need to download. In the code you will find :



- The first time series analysis with varicelle data (ts, plot, ggseasonplot)



- Exponential smoothing on varicelle data (window,  HoltWInters, predict, plot)



- The second time series analysis with maunaloa2Co2 data (ts, autoplot, seq, lm, coef, box.test, plot, diff, decompose, ggAcf)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
